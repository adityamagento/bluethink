<?php

namespace Bluethink\Yogesh\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class CheckoutCartProductAddAfterObserver implements ObserverInterface
{

    protected $_request;
    protected $logger;
    protected $productFactory;

    /**
     * @param RequestInterface $request
     */
    public function __construct(
        RequestInterface $request,
        \Bluethink\Yogesh\Logger\CodilarLogger $logger,
        \Magento\Catalog\Model\ProductFactory $productFactory
    ){
            $this->_request = $request;
            $this->logger = $logger;
            $this->productFactory = $productFactory;
    }

    /**
     * @param EventObserver $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        /* @var \Magento\Quote\Model\Quote\Item $item */
        $item = $observer->getQuoteItem();

        // $this->logger->info('+observer+');
        $additionalOptions = array();

        if ($additionalOption = $item->getOptionByCode('additional_options')){
            $additionalOptions = (array) unserialize($additionalOption->getValue());
        }

        $post = $this->_request->getParam('bluethink');
        
        // set attribute value for catalog_product_entity_text table
        $product = $this->productFactory->create();
        $prod = $product->loadByAttribute('sku', $item->getSku());
        $attr_code = 'delivery_instruction';
        $value = $post['delivery_instruction'];
        $prod->setCustomAttribute($attr_code, $value);
        $prod->save();

        if(is_array($post)){
            foreach($post as $key => $value){
                if($key == '' || $value == ''){
                    continue;
                }

                $additionalOptions[] = [
                    'label' => $key,
                    'value' => $value
                ];
            }
        }

        if(is_array($additionalOptions) && count($additionalOptions) > 0){
            $item->addOption(array(
                'code' => 'additional_options',
                'value' => json_encode($additionalOptions)
            ));
        }

    }
}
